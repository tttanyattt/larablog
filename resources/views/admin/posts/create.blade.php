@extends('admin.layouts.main')

@section('styles')
    {!! Html::style('/plugins/almasaeed2010/adminlte/plugins/select2/css/select2.min.css') !!}
    {!! Html::style('/plugins/almasaeed2010/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') !!}
    {!! Html::style('/plugins/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.css') !!}
    {!! Html::style('/plugins/almasaeed2010/adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') !!}
    {!! Html::style('/plugins/almasaeed2010/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') !!}
    {!! Html::style('/plugins/almasaeed2010/adminlte/plugins/summernote/summernote-bs4.css') !!}
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{ $breadcrumbs[1] }}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('posts.index') }}">{{ $breadcrumbs[0] }}</a></li>
                            <li class="breadcrumb-item active">{{ $breadcrumbs[1] }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            {!! Form::open(['route' => 'posts.store', 'id' => 'quickForm', 'files'=>true]) !!}
                            <div class="card-body">
                                @include('admin.layouts.message')
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" placeholder="Введите название">
                                </div>
                                <div class="form-group">
                                    <label for="image">Изображение</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="avatar">Выбрать файл</label>
                                            <input type="file" class="custom-file-input" id="image" name="image">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Категория</label>
                                    {!! Form::select('category_id', $categories, null, [
                                        'class' => "form-control select2",
                                        'placeholder' => 'Выберите категорию',
                                        'style' => "width: 100%;"
                                    ]) !!}
                                </div>
                                <div class="form-group">
                                    <label>Теги</label>
                                    {!! Form::select('tags[]', $tags, null, [
                                        'class' => "select2",
                                        'data-placeholder' => 'Выберите теги',
                                        'multiple' => "multiple",
                                        'style' => "width: 100%;"
                                    ]) !!}
                                </div>
                                <div class="form-group">
                                    <label>Дата:</label>
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <input type="text" name="date" value="{{old('date', $date)}}" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        {{ Form::checkbox('is_featured', '1', old('is_featured'), ['id'=>"is_featured", 'class'=>"custom-control-input"]) }}
                                        <label class="custom-control-label" for="is_featured">Рекомендовать</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        {{ Form::checkbox('status', '1', old('status'), ['id'=>"status", 'class'=>"custom-control-input"]) }}
{{--                                        <input type="checkbox" name="status" value="{{old('status')}}" id="status" class="custom-control-input">--}}
                                        <label class="custom-control-label" for="status">Черновик</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Описание статьи</label>
                                    <div class="mb-3">
                                    <textarea name="description" class="textarea" placeholder=""
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('description')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Текст статьи</label>
                                    <div class="mb-3">
                                    <textarea name="content" class="textarea" placeholder=""
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('content')}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-secondary">Создать</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/jquery-validation/jquery.validate.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/jquery-validation/additional-methods.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/select2/js/select2.full.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/moment/moment.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/daterangepicker/daterangepicker.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/summernote/summernote-bs4.min.js') !!}
    {!! Html::script('/js/almasaeed2010/admin/posts/adminLTE-field-validation.js') !!}
@endsection
