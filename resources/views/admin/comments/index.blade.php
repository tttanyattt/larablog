@extends('admin.layouts.main')

@section('styles')
    {!! Html::style('/plugins/almasaeed2010/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') !!}
    {!! Html::style('/plugins/almasaeed2010/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') !!}
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{ $breadcrumbs[0] }}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item active">{{ $breadcrumbs[0] }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @include('admin.layouts.message')
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Текст</th>
                                        <th>Пост</th>
                                        <th data-priority="1">Запретить/Разрешить</th>
                                        <th>Удалить</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($comments as $comment)
                                        <tr>
                                            <td>{{ $comment->id }}</td>
                                            <td>{{ $comment->text }}</td>
                                            <td><a target="_blank" href="{{route('post.show', $comment->post->slug)}}">{{route('post.show', $comment->post->slug)}}</a></td>
                                            <td>
                                                @if($comment->status == \App\Comment::IS_ACTIVE)
                                                    <a href="/admin/comments/toggle/{{$comment->id}}" class="btn btn-warning">Скрыть</a>
                                                @else
                                                    <a href="/admin/comments/toggle/{{$comment->id}}" class="btn btn-success">Показать</a>
                                                @endif
                                            </td>
                                            <td>
                                                {{ Form::open([
                                                    'route'=>['comments.destroy', $comment->id],
                                                    'method'=>'delete'
                                                ]) }}
                                                <button onclick="return confirm('Вы уверены, что хотите удалить?')" type="submit" class="btn btn-danger">
                                                    Удалить
                                                </button>
                                                {{ Form::close() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Текст</th>
                                        <th>Пост</th>
                                        <th>Запретить/Разрешить</th>
                                        <th>Удалить</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/dist/js/demo.js') !!}
    {!! Html::script('/js/almasaeed2010/adminLTE-table.js') !!}
@endsection
