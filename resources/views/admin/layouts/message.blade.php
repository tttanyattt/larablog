@if ($errors->any())
    <div class="alert alert-warning alert-dismissible">
        @foreach($errors->all() as $error)
            <p>{{ $error  }}</p>
        @endforeach
    </div>
@elseif (Session::has('alert-warning'))
    <div class="alert alert-warning alert-dismissible">
        <p>{{ Session::get('alert-warning') }}</p>
    </div>
@elseif (Session::has('alert-success'))
    <div class="alert alert-success alert-dismissible">
        <p>{{ Session::get('alert-success') }}</p>
    </div>
@elseif (Session::has('alert-info'))
    <div class="alert alert-info alert-dismissible">
        <p>{{ Session::get('alert-info') }}</p>
    </div>
@endif
