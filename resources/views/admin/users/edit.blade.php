@extends('admin.layouts.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{ $breadcrumbs[1] }}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">{{ $breadcrumbs[0] }}</a></li>
                            <li class="breadcrumb-item active">{{ $breadcrumbs[1] }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            {!! Form::open([
                                'route' => ['users.update', $user->id],
                                'id'    => 'quickForm',
                                'files' => true,
                                'method' => 'put'
                            ])!!}
                            <div class="card-body">
                                @include('admin.layouts.message')
                                <div class="form-group">
                                    <img class="img-preview" src="{{ $user->getImage()  }}">
                                </div>
                                <div class="form-group">
                                    <label for="name">Имя</label>
                                    <input type="text" name="name" value="{{ $user->name }}" class="form-control" id="name" placeholder="Введите имя">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" value="{{ $user->email }}" class="form-control" id="email" placeholder="Введите email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Пароль</label>
                                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label for="avatar">Aватар</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="avatar">Выбрать файл</label>
                                            <input type="file" class="custom-file-input" id="avatar" name="avatar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-secondary">Сохранить</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/jquery-validation/jquery.validate.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/jquery-validation/additional-methods.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') !!}
    {!! Html::script('/js/almasaeed2010/admin/users/edit/adminLTE-field-validation.js') !!}
@endsection
+
