@extends('admin.layouts.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{ $breadcrumbs[1] }}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Главная</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">{{ $breadcrumbs[0] }}</a></li>
                            <li class="breadcrumb-item active">{{ $breadcrumbs[1] }}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            {!! Form::open(['route' => 'categories.store', 'id' => 'quickForm']) !!}
                                <div class="card-body">
                                    @include('admin.layouts.message')
                                    <div class="form-group">
                                        <label for="title">Название категории</label>
                                        <input type="text" name="title" class="form-control" id="title" placeholder="Введите название категории">
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-secondary">Создать</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/jquery-validation/jquery.validate.min.js') !!}
    {!! Html::script('/plugins/almasaeed2010/adminlte/plugins/jquery-validation/additional-methods.min.js') !!}
    {!! Html::script('/js/almasaeed2010/admin/categories/adminLTE-field-validation.js') !!}
@endsection
