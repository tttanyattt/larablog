@extends('errors.full')

@section('styles')
    {!! Html::style('/css/error.css') !!}
@endsection

@section('content')
    <div class="col-md-12">
        <div class="error-wrapper">
            <div class="cont_principal">
                <div class="cont_error">
                    <h1>404</h1>
                    <a href="/" class="button">Вернуться назад</a>
                </div>
                <div class="cont_aura_1"></div>
                <div class="cont_aura_2"></div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    {!! Html::script('/js/error.js') !!}
@endsection
