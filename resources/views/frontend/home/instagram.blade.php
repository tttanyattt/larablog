<div id="footer">
    <div class="footer-instagram-section">
        <h3 class="footer-instagram-title text-center text-uppercase">Instagram</h3>

        <div id="footer-instagram" class="owl-carousel">

            <div class="item">
                <a href="#"><img src="/img/ins-1.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/img/ins-2.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/img/ins-3.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/img/ins-4.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/img/ins-5.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/img/ins-6.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/img/ins-7.jpg" alt=""></a>
            </div>
            <div class="item">
                <a href="#"><img src="/img/ins-8.jpg" alt=""></a>
            </div>

        </div>
    </div>
</div>
