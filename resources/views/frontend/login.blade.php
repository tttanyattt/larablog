@extends('frontend.layouts.main')

@section('content')
    <div class="col-md-8">
        <div class="leave-comment mr0">
            <h3 class="text-uppercase">Авторизация</h3>
            <br>
                @include('admin.layouts.message')
                {!! Form::open([
                   'route' => 'login',
                   'method'=> 'post',
                   'id'    => 'quickForm',
                   'class' => 'form-horizontal contact-form'
               ]) !!}
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" value="{{old('email')}}" class="form-control" id="email" name="email"
                               placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="Пароль">
                    </div>
                </div>
                <button type="submit" name="submit" class="btn send-btn">Войти</button>
                {!! Form::close() !!}
        </div>
    </div>
@endsection
