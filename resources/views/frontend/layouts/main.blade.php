<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Blog</title>
    <link rel="icon" type="image/png" href="/img/favicon.png">

    <link rel="stylesheet" href="/css/front.css">
    @yield('styles')
</head>
<body>

<!--navbar-->
@include('frontend.layouts.navbar')

<!--main content start-->

<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('admin.layouts.message')
            </div>
            @yield('content')
            @include('frontend.layouts.sidebar.main')
        </div>
    </div>
</div>
<!-- end main content-->

<!--footer start-->
@include('frontend.layouts.footer')

<script src="/js/front.js"></script>
@yield('scripts')
@yield('page-scripts')

</body>
</html>
