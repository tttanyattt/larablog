<footer class="footer-widget-section">
    <div class="footer-copy">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">&copy; {{date("Y")}} <a href="/">{{env("PROJECT_NAME")}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
