<aside class="widget news-letter">
    <h3 class="widget-title text-uppercase text-center">Get Newsletter</h3>
    <form action="/subscribe" method="post">
        {{csrf_field()}}
        <input type="text" placeholder="Your email address" name="email">
        <input type="submit" value="Subscribe Now"
               class="text-uppercase text-center btn btn-subscribe">
    </form>
</aside>
