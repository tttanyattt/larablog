{{--@if(!empty($recentPosts))--}}
{{--<aside class="widget pos-padding">--}}
{{--    <h3 class="widget-title text-uppercase text-center">Недавнее</h3>--}}
{{--    @foreach($recentPosts as $post)--}}
{{--    <div class="thumb-latest-posts">--}}
{{--        <div class="media">--}}
{{--            <div class="media-left">--}}
{{--                <a href="{{route('post.show', $post->slug)}}" class="popular-img"><img src="{{$post->getImage()}}" alt="">--}}
{{--                    <div class="p-overlay"></div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="p-content">--}}
{{--                <a href="{{route('post.show', $post->slug)}}" class="text-uppercase">{{$post->title}}</a>--}}
{{--                <span class="p-date">{{$post->getDate()}}</span>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    @endforeach--}}
{{--</aside>--}}
{{--@endif--}}

@if(!empty($recentPosts))
    <aside class="widget">
{{--        <h3 class="widget-title text-uppercase text-center">Недавнее</h3>--}}
        @foreach($recentPosts as $post)
            <div class="popular-post">
                <a href="{{route('post.show', $post->slug)}}" class="popular-img">
                    <img src="{{$post->getImage()}}" alt="">
                    <div class="p-overlay"></div>
                </a>
                <div class="p-content">
                    <a href="{{route('post.show', $post->slug)}}" class="text-uppercase">{{$post->title}}</a>
                    {{--            <span class="p-date">{{$post->getDate()}}</span>--}}
                </div>
            </div>
        @endforeach
    </aside>
@endif
