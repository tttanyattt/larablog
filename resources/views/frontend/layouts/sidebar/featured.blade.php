@if(!$featuredPosts->isEmpty())
<aside class="widget">
    <h3 class="widget-title text-uppercase text-center">Featured Posts</h3>
    <div id="widget-feature" class="owl-carousel">
        @foreach($featuredPosts as $post)
        <div class="item">
            <div class="feature-content">
                <img src="{{$post->getImage()}}" alt="">
                <a href="{{route('post.show', $post->slug)}}" class="overlay-text text-center">
                    <h5 class="text-uppercase">{{$post->title}}</h5>
                    <p>{!! $post->description !!}</p>
                </a>
            </div>
        </div>
        @endforeach
    </div>
</aside>
@endif
