@extends('frontend.layouts.main')

@section('content')
    <div class="col-md-8">
        <div class="card">
            {!! Form::open([
                'route' => ['users.update', $user->id],
                'id'    => 'quickForm',
                'files' => true,
                'method' => 'put'
            ])!!}
            <div class="card-body">
                @include('admin.layouts.message')
                <div class="form-group">
                    <img class="img-preview" src="{{ $user->getImage()  }}">
                </div>
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input type="text" name="name" value="{{ $user->name }}" class="form-control" id="name" placeholder="Введите имя">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" value="{{ $user->email }}" class="form-control" id="email" placeholder="Введите email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Пароль</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="avatar">Aватар</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <label class="custom-file-label" for="avatar">Выбрать файл</label>
                            <input type="file" class="custom-file-input" id="avatar" name="avatar">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-secondary">Сохранить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
