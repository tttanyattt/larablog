<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/post/{slug}', 'HomeController@post')->name('post.show');
Route::get('/tag/{slug}', 'HomeController@tag')->name('tag.show');
Route::get('/category/{slug}', 'HomeController@category')->name('category.show');
Route::post('/comment', 'CommentsController@store');
//Route::post('/subscribe', 'SubscribeController@store');
//Route::get('/verify/{token}', 'SubsController@verify');

Route::group([
        'middleware' => 'guest'
    ],
    function() {
//        Route::get('/register', 'AuthController@registerForm')->name('register');
//        Route::post('/register', 'AuthController@register');
        Route::get('/login', 'AuthController@loginForm')->name('login');
        Route::post('/login', 'AuthController@login');
    }
);

Route::group([
        'middleware' => 'auth'
    ],
    function() {
        Route::get('/logout', 'AuthController@logout');
//        Route::get('/profile', 'ProfileController@index');
//        Route::post('/profile', 'ProfileController@store');
    }
);

Route::group([
        'prefix'=>'admin',
        'namespace'=>'Admin',
        'middleware' => 'admin'
    ],
    function() {
        Route::get('/', 'DashboardController@index');
        Route::get('/comments', 'CommentsController@index')->name('comments.index');
        Route::get('/comments/toggle/{id}', 'CommentsController@toggle');
        Route::delete('/comments/{id}/destroy', 'CommentsController@destroy')->name('comments.destroy');
        Route::resources([
            '/categories' => 'CategoriesController',
            '/tags' => 'TagsController',
            '/users' => 'UsersController',
            '/posts' => 'PostsController',
            '/subscribers' => 'SubscribersController',
        ]);
});


