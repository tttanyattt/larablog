$(function () {
    //Initialize Select2 Elements
    $('.select2').select2();

    // Date range picker
    $('#reservationdate').datetimepicker({
        format:'DD/MM/YYYY'
    });

    $('.textarea').summernote();
    jQuery.validator.setDefaults({
        // This will ignore all hidden elements alongside `contenteditable` elements
        // that have no `name` attribute
        ignore: ":hidden, [contenteditable='true']:not([name])"
    });

    //Validate Form
    bsCustomFileInput.init();
    $('#quickForm').validate({
        rules: {
            title: {
                required: true
            },
            date: {
                required: true
            },
        },
        messages: {
            title: {
                required: "Please enter a title"
            },
            date: {
                required: "Please enter a date"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});
