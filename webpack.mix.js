const mix = require('laravel-mix');

// *** Admin ***
mix.sass('resources/sass/admin-style.scss', 'public/css');
mix.sass('resources/sass/frontend-style.scss', 'public/css');
mix.sass('resources/sass/error.scss', 'public/css');

mix.styles([
    'vendor/almasaeed2010/adminlte/dist/css/adminlte.min.css',
    'vendor/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css',
    'vendor/almasaeed2010/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
    'public/css/admin-style.css'
], 'public/css/admin.css');

mix.combine([
    'vendor/almasaeed2010/adminlte/plugins/jquery/jquery.min.js',
    'vendor/almasaeed2010/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js',
    'vendor/almasaeed2010/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
    'vendor/almasaeed2010/adminlte/dist/js/adminlte.js',
    'vendor/almasaeed2010/adminlte/dist/js/demo.js',
], 'public/js/admin.js');

// Vendor
mix.copy('vendor/almasaeed2010/adminlte/plugins/', 'public/plugins/almasaeed2010/adminlte/plugins/');
mix.copy('vendor/almasaeed2010/adminlte/build/', 'public/plugins/almasaeed2010/adminlte/build/');
mix.copy('vendor/almasaeed2010/adminlte/dist/', 'public/plugins/almasaeed2010/adminlte/dist/');
mix.copy('vendor/almasaeed2010/adminlte/plugins/fontawesome-free/webfonts', 'public/webfonts');
mix.copy('vendor/almasaeed2010/adminlte/dist/css/adminlte.min.css.map', 'public/css/adminlte.min.css.map');

// Resources
mix.copy('resources/img', 'public/img');
mix.copy('resources/js/almasaeed2010/', 'public/js/almasaeed2010/');

// *** Frontend ***

mix.styles([
    'resources/css/bootstrap.min.css',
    'resources/css/font-awesome.min.css',
    'resources/css/animate.min.css',
    'resources/css/owl.carousel.css',
    'resources/css/owl.theme.css',
    'resources/css/owl.transitions.css',
    'resources/css/style.css',
    'resources/css/responsive.css',
    'public/css/frontend-style.css',
], 'public/css/front.css');

mix.combine([
    'resources/js/jquery-1.11.3.min.js',
    'resources/js/bootstrap.min.js',
    'resources/js/owl.carousel.min.js',
    'resources/js/jquery.stickit.min.js',
    'resources/js/menu.js',
    'resources/js/scripts.js',
], 'public/js/front.js');

mix.copy('resources/fonts', 'public/fonts');
mix.copy('resources/css', 'public/css');
mix.copy('resources/js', 'public/js');
