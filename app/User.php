<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    const IS_REGULAR = 0;
    const IS_ADMIN = 1;
    const IS_BANNED = 0;
    const IS_ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public static function add($fields)
    {
        $user = new static;
        $user->fill($fields);
        $user->save();

        return $user;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()
    {
        $this->removeAvatar();
        $this->delete();
    }

    public function generatePassword($password)
    {
        if(!empty($password)) {
            $this->password = bcrypt($password);
            $this->save();
        }
    }

    public function uploadAvatar($image)
    {
        if($image == null) { return; }

        if(!empty($this->avatar)) {
            Storage::delete('uploads/avatar/' . $this->avatar);
        }

        $filename = Str::random(10) . '.' . $image->extension();
        // folder: public/uploads
        $image->storeAs('uploads/avatar', $filename);
        $this->avatar = $filename;
        $this->save();
    }

    public function removeAvatar()
    {
        if(!empty($this->avatar)) {
            Storage::delete('uploads/avatar/' . $this->avatar);
        }
    }

    public function getImage()
    {
        if (!$this->avatar) {
            return '/img/no-user-image.jpg';
        }
        //todo check if exists file on server
        return '/uploads/avatar/' . $this->avatar;
    }

    public function makeAdminUser()
    {
        $this->is_admin = self::IS_ADMIN;
        $this->save();
    }

    public function makeRegularUser()
    {
        $this->is_admin = self::IS_REGULAR;
        $this->save();
    }

    public function toggleAdmin($value)
    {
        if($value == null) {
            return $this->makeRegularUser();
        }

        return $this->makeAdminUser();
    }

    public function ban()
    {
        $this->status = self::IS_BANNED;
        $this->save();
    }

    public function unban()
    {
        $this->status = self::IS_ACTIVE;
        $this->save();
    }

    public function toggleBan($value)
    {
        if($value == null) {
            return $this->unban();
        }

        return $this->ban();
    }

    public function showIfAdmin()
    {
        if($this->is_admin == self::IS_ADMIN) {
            return 'Админ';
        }
        return 'Пользователь';
    }

    public function showStatus()
    {
        if($this->status == self::IS_ACTIVE) {
            return 'Активен';
        }
        return 'Забанен';
    }
}
