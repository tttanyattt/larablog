<?php

namespace App\Http\Controllers;

use App\Subscribtion;
use App\Mail\SubscribeEmail;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:subscribtions'
        ]);

        $subs = Subscribtion::add($request->get('email'));
        $subs->generateToken();

        \Mail::to($subs)->send(new SubscribeEmail($subs));

        return redirect()->back()->with('alert-info', 'Проверьте Вашу почту!');
    }

    public function verify($token)
    {
        $subs = Subscribtion::where('token', $token)->firstOrFail();
        $subs->token = null;
        $subs->save();

        return redirect('/')->with('alert-success', 'Ваша почта успешно подтверждена!');
    }
}
