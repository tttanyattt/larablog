<?php

namespace App\Http\Controllers;

use Auth;
use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        $comment = new Comment;
        $comment->text = $request->get('message');
        $comment->post_id = $request->get('post_id');
//dd($request->all());
        if(Auth::check()) {
            $comment->user_id = Auth::user()->id;
        } else {
            $comment->user_name = $request->get('name');
            $comment->email = $request->get('email');
        }
        $comment->save();

        //todo если комментарий добавил админ, то подтверждение не нужно
        return redirect()->back()->with('alert-success', 'Ваш комментарий принят и будет проверен модератором.');
    }
}
