<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::where('status', Post::STATUS_IS_PUBLIC)->paginate(3);

        return view('frontend.home.index', compact(
            'posts'
        ));
    }

    public function post($slug)
    {
        //todo public post show else on to home
        $post = Post::where('slug', $slug)->firstOrFail();
        return view('frontend.post', ['post' => $post]);
    }

    public function tag($slug)
    {
        $tag = Tag::where('slug', $slug)->firstOrFail();
        $posts = $tag->posts()->where('status', Post::STATUS_IS_PUBLIC)->paginate(2);
        return view('frontend.list', ['posts' => $posts]);
    }

    public function category($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $posts = $category->posts()->where('status', Post::STATUS_IS_PUBLIC)->paginate(2);
        return view('frontend.list', ['posts' => $posts]);
    }
}
