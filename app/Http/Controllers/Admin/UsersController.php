<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    const CATEGORY_NAME = 'Пользователи';

    public function index()
    {
        $users = User::all();
        return view('admin.users.index', [
            'users' => $users,
            'breadcrumbs' => [self::CATEGORY_NAME]
        ]);
    }

    public function create()
    {
        return view('admin.users.create', [
            'breadcrumbs' => [self::CATEGORY_NAME, 'Создание пользователя']
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'avatar' => 'nullable|image',
        ]);

        $user = User::add($request->all());
        $user->generatePassword($request->get('password'));
        $user->uploadAvatar($request->file('avatar'));

        $request->session()->flash('alert-success', 'User was successfully added!');
        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.edit', [
            'user'=>$user,
            'breadcrumbs' => [self::CATEGORY_NAME, 'Редактирование пользователя']
        ]);
    }

    public function update(Request $request, $id)
    {
        // todo delete only avatar image

        $user = User::find($id);

        $this->validate($request, [
            'name' => 'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id),
            ],
            'avatar' => 'nullable|image',
        ]);

        $user->edit($request->all());
        $user->generatePassword($request->get('password'));
        $user->uploadAvatar($request->file('avatar'));

        $request->session()->flash('alert-success', 'User was successfully updated!');
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        User::find($id)->remove();

        // todo message
//        $request->session()->flash('alert-success', 'User was successfully updated!');
        return redirect()->route('users.index');
    }
}
