<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Subscribtion;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{
    const CATEGORY_NAME = 'Подписчики';

    public function index()
    {
        $subs = Subscribtion::all();

        return view('admin.subs.index', ['subs' => $subs, 'breadcrumbs' => [self::CATEGORY_NAME]]);
    }

    public function create()
    {
        return view('admin.subs.create', [
            'breadcrumbs' => [self::CATEGORY_NAME, 'Создание подписчика']
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:subscribtions'
        ]);

        Subscribtion::add($request->get('email'));

        $request->session()->flash('alert-success', 'Подписчик был успешно добавлен!');
        return redirect()->route('subscribers.index');
    }

    public function destroy($id)
    {
        $category = Subscribtion::find($id)->delete();
        return redirect()->route('subscribers.index');
    }
}
