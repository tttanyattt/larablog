<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
//        $imgUrl = '/img/almasaeed2010/adminlte';
        $imgUrl = '/plugins/almasaeed2010/adminlte/dist/img';
        return view('admin.dashboard', ['imgUrl' => $imgUrl]);
    }
}
