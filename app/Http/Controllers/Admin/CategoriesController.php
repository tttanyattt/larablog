<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    const CATEGORY_NAME = 'Категории';

    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index', [
            'categories' => $categories,
            'breadcrumbs' => [self::CATEGORY_NAME]
        ]);
    }

    public function create()
    {
        return view('admin.categories.create', [
            'breadcrumbs' => [self::CATEGORY_NAME, 'Создание категории']
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        Category::create($request->all());

        $request->session()->flash('alert-success', 'Категория была успешно добавлена!');
        return redirect()->route('categories.index');;
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit', [
            'category'=>$category,
            'breadcrumbs' => [self::CATEGORY_NAME, 'Редактирование категории']
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $category = Category::find($id);
        $category->update($request->all());

        $request->session()->flash('alert-success', 'Категория была успешно обновлена!');
        return redirect()->route('categories.index');
    }

    public function destroy($id)
    {
        $category = Category::find($id)->delete();
        return redirect()->route('categories.index');
    }
}
