<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    const CATEGORY_NAME = 'Комментарии';

    public function index()
    {
        $comments = Comment::all()->sortBy("status");

        return view('admin.comments.index', ['comments' => $comments, 'breadcrumbs' => [self::CATEGORY_NAME]]);
    }

    public function toggle($id)
    {
        $comment = Comment::find($id);
        $comment->toggleStatus();

        return redirect()->back();
    }

    public function destroy($id)
    {
        Comment::find($id)->remove();

        return redirect()->back();
    }
}
