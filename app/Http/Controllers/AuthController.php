<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function registerForm()
    {
        return view('frontend.register');
    }

    public function loginForm()
    {
        return view('frontend.login');
    }

    public function register(Request $request)
    {
        // todo сделать подтверждение емейла по почте
        $this->validate($request, [
             'name' => 'required|unique:users|string|min:3|max:255',
             'email' => 'required|email|unique:users',
             'password' => 'required|string|min:5|max:255',
        ]);

        $user = User::add($request->all());
        $user->generatePassword($request->get('password'));

        return redirect('/login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ])) {
            return redirect('/');
        }

        return redirect()->back()->with('alert-warning', 'Неверный логин или пароль');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
