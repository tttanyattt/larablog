<?php

namespace App\Providers;

use App\Comment;
use App\Post;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, 'ru_RU.UTF-8');
        Carbon::setLocale(config('app.locale'));

        view()->composer('frontend.layouts.sidebar.main', function($view) {
            $view->with('popularPosts', Post::getPopularPosts());
            $view->with('featuredPosts', Post::getFeaturedPosts());
            $view->with('recentPosts', Post::getRecentPosts());
            $view->with('categories', Post::getAllCategories());
        });

        view()->composer('admin.layouts.sidebar', function($view) {
            $view->with('newCommentsCount', Comment::getNewCommentsCount());
        });
    }
}
