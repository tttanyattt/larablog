<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Str;

class Post extends Model
{
    use Sluggable;

    const STATUS_IS_DRAFT = 0;
    const STATUS_IS_PUBLIC = 1;
    const POST_IS_FEATURED = 1;
    const POST_IS_STANDARD = 0;

    protected $fillable = ['title', 'content', 'date', 'description'];

    public function category()
    {
        // fk: category_id
        return $this->belongsTo(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function tags()
    {
        // many to many
        return $this->belongsToMany(
            Tag::class,
            'post_tags',
            'post_id',
            'tag_id'
        );
    }

    /*
     * Convert string without dublications:
     * "привет" to "privet"
     * "привет" to "privet-1"
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function add($fields)
    {
        $post = new static;
        $post->fill($fields);
        $post->user_id = Auth::user()->id;
        $post->save();

        return $post;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()
    {
        $this->removeImage();
        $this->delete();
    }

    public function removeImage()
    {
        if(!empty($this->image)) {
            Storage::delete('uploads/'.$this->image);
        }
    }

    public function getImage()
    {
        if (empty($this->image)) {
            return '/img/no-image.jpg';
        }

        return '/uploads/' . $this->image;
    }

    public function uploadImage($image)
    {
        if($image == null) { return; }

        $this->removeImage();
        $filename = Str::random(10) . '.' . $image->extension();
        // folder: public/uploads
        $image->storeAs('uploads', $filename);
        $this->image = $filename;
        $this->save();
    }

    public function setCategory($id)
    {
        if($id == null) {return;}
        $this->category_id = $id;
        $this->save();
    }

    public function setTags($ids)
    {
        if($ids == null) { return; }

        $this->tags()->sync($ids);
    }

    public function setDraft()
    {
        $this->status = self::STATUS_IS_DRAFT;
        $this->save();
    }

    public function setPublic()
    {
        $this->status = self::STATUS_IS_PUBLIC;
        $this->save();
    }

    public function toggleStatus($value)
    {
        if ($value == null) {
            return $this->setPublic();
        }
        return $this->setDraft();
    }

    public function setFeatured()
    {
        $this->is_featured = self::POST_IS_FEATURED;
        $this->save();
    }

    public function setStandard()
    {
        $this->is_featured = self::POST_IS_STANDARD;
        $this->save();
    }

    public function toggleFeatured($value)
    {
        if ($value == null) {
            return $this->setStandard();
        }

        return $this->setFeatured();
    }

    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;
    }

    public function getDateAttribute($value)
    {
        $date = Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
        return $date;
    }

    public function getTagsTitles()
    {
        if (!$this->tags->isEmpty()) {
            return implode(', ', $this->tags->pluck('title')->sort()->all());
        }

        return 'Нет тегов';
    }

    public function getCategoryTitle()
    {
        if(!empty($this->category)) {
            return $this->category->title;
        }

        return 'Нет категории';
    }

    public function getCategoryId()
    {
        return $this->category != null ? $this->category->id : null;
    }

    public function hasCategory()
    {
        return $this->category != null ? true : false;
    }

    public function getDate()
    {
        //February 12, 2016
        $date = Carbon::createFromFormat('d/m/Y', $this->date)->format('F d, Y');
        return $date;
    }

    public function hasPrevious()
    {
        return self::where('id', '<', $this->id)->max('id');
    }

    public function getPrevious()
    {
        $postId = $this->hasPrevious();
        return self::find($postId);
    }

    public function hasNext()
    {
        return self::where('id', '>', $this->id)->min('id');
    }

    public function getNext()
    {
        $postId = $this->hasNext();
        return self::find($postId);
    }

    public function related()
    {
        return self::all()->except($this->id);
    }

    public static function getPopularPosts()
    {
        return Post::orderBy('views', 'desc')->take(3)->get();
    }

    public static function getFeaturedPosts()
    {
        return Post::where('is_featured', Post::POST_IS_FEATURED)->take(3)->get();
    }

    public static function getRecentPosts()
    {
        return Post::orderBy('date', 'desc')->take(4)->get();
    }

    public static function getAllCategories()
    {
        return Category::orderBy('title')->get();
    }

    public function getComments()
    {
        return $this->comments()->where('status', Comment::IS_ACTIVE)->get();
    }
}
