<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    const IS_ACTIVE = 1;
    const IS_NOT_ACTIVE = 0;

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function allow()
    {
        $this->status = self::IS_ACTIVE;
        $this->save();
    }

    public function disallow()
    {
        $this->status = self::IS_NOT_ACTIVE;
        $this->save();
    }

    public function toggleStatus()
    {
        if(!$this->status) {
            return $this->allow();
        }

        return $this->disallow();
    }

    public function remove()
    {
        $this->delete();
    }

    public static function getNewCommentsCount()
    {
        return Comment::where('status', Comment::IS_NOT_ACTIVE)->count();
    }
}
